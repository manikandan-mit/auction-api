package com.mani.project.auction.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Entity(name = "item_status")
@Getter @Setter
public class StatusBO {
	
	@Id
	@Column(name = "status_id")
	private Integer statusId;
	
	@Column(name = "status_name")
	private String status_name;
}
