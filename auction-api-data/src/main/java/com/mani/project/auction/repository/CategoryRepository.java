package com.mani.project.auction.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mani.project.auction.entity.CategoryBO;

@Repository
public interface CategoryRepository extends CrudRepository<CategoryBO, Integer> {
	
	List<CategoryBO> findAll();
	
	@Transactional
	@Modifying
	@Query(value = "update category s set s.active = :activeStatus where s.category_id = :category", nativeQuery = true)
	public void updateActiveStatus(@Param("category") int categoryId, @Param("activeStatus") boolean activeStatus);


}
