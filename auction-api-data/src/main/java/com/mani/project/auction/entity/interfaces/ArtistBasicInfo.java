package com.mani.project.auction.entity.interfaces;

public interface ArtistBasicInfo {
	Integer getArtistId();
	String getArtistName();
	Boolean getActive();
}
