package com.mani.project.auction.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "banner")
@Getter @Setter
public class BannerBO implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID")
	private Integer id;
	
	@Column(name="URL")
	private String url;
	
	@Column(name="PUBLIC_ID")
	private String publicId;
	
	@Column(name="FILE_NAME")
	private String fileName;
	
	@Column(name="FILE_FORMAT")
	private String fileExtension;
}
