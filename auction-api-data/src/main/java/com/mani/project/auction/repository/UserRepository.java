package com.mani.project.auction.repository;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.mani.project.auction.entity.UserBO;
import com.mani.project.auction.entity.UserRole;

public interface UserRepository extends CrudRepository<UserBO, String>{

	Optional<UserBO> findByUserId(String userId);
	Optional<UserBO> findByUsername(String username);
	boolean existsByUserId(String userId);
	boolean existsByUsername(String username);
	boolean existsByEmail(String email);
	boolean existsByPhone(String phone);
	List<UserBO> findByRolesRoleName(UserRole role);
	
	@Transactional
	@Modifying
	@Query(value = "Update users u set u.active = :activeStatus where u.user_id = :userId", nativeQuery = true)
	void updateActiveStatusOfUser(@Param("userId") String userId, @Param("activeStatus") boolean activeStatus);

}
