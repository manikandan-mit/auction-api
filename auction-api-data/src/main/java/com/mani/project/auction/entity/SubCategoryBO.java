package com.mani.project.auction.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "sub_category")
@Getter @Setter
@NamedQueries(value = {
		@NamedQuery(name = "SubCategoryBO.listAllActiveSubCategory", query = "select s.subCategoryId, s.subCategoryName from SubCategoryBO s WHERE s.isActive=true")
})
public class SubCategoryBO implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sub_category_id")
	private Integer subCategoryId;
	
	@Column(name = "sub_category_name")
	private String subCategoryName;
	
	@Column(name = "priority")
	private Integer order;

	@Column(name = "active")
	private Boolean isActive;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "category_id", nullable = false)
	@JsonIgnore
	private CategoryBO category;

}
