package com.mani.project.auction.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mani.project.auction.entity.BannerBO;

@Repository
public interface BannerRepository extends CrudRepository<BannerBO, Integer>{
	
	List<BannerBO> findAll();
}
