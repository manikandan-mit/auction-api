package com.mani.project.auction.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "artist")
@Getter @Setter
@NamedQueries(value = {
		@NamedQuery(name = "ArtistBO.listAllActiveArtist", query = "select a.artistId, a.artistName from ArtistBO a WHERE a.active=true")
})
public class ArtistBO  implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "artist_id")
	private Integer artistId;
	
	@Column(name = "artist_name")
	private String artistName;
	
	@Column(name = "artist_desc")
	private String artistDesc;

	@Column(name = "profile_url")
	private String artistProfileUrl;

	@Column(name = "profile_public_id")
	private String artistPublicId;

	@Column(name = "active")
	private Boolean active;
	
}
