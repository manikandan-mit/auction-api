package com.mani.project.auction.entity;

public enum UserRole {
	USER,
	ADMIN,
	OTHERS
}
