package com.mani.project.auction.entity;


import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "items")
@Getter @Setter
@NamedQueries(value = {
		@NamedQuery(name = "ProductBO.findAllProducts", query = "SELECT p FROM ProductBO p WHERE p.status.statusId = 1"),
		@NamedQuery(name = "ProductBO.listAllProducts", query = "SELECT p.productId, p.productName, p.auctionDate FROM ProductBO p WHERE p.status.statusId = :status"),
		@NamedQuery(name = "ProductBO.findProductsByArtistAndCategory", query = "SELECT p FROM ProductBO p WHERE p.status.statusId = 1 and p.artist.artistId in :artist and p.artist.active = true and p.category.subCategoryId in :category and p.category.isActive = true"),
		@NamedQuery(name = "ProductBO.findProductsByCategory", query = "SELECT p FROM ProductBO p WHERE p.status.statusId = 1 and p.category.subCategoryId in :category and p.category.isActive = true"),
		@NamedQuery(name = "ProductBO.findProductsByArtist", query = "SELECT p FROM ProductBO p WHERE p.status.statusId = 1 and p.artist.artistId in :artist and p.artist.active = true"),
		@NamedQuery(name = "ProductBO.updateProductStatus", query = "update ProductBO p set p.status.statusId = :status WHERE p.productId in (:productId)"),
		@NamedQuery(name = "ProductBO.updateProductName", query = "update ProductBO p set p.productName = :productName WHERE p.productId = :productId"),
		@NamedQuery(name = "ProductBO.updateProductDesc", query = "update ProductBO p set p.productDescription = :productDescription WHERE p.productId = :productId"),
		@NamedQuery(name = "ProductBO.updateProductEstimate", query = "update ProductBO p set p.estimatedMin = :estimatedMin, p.estimatedMax = :estimatedMax WHERE p.productId = :productId"),
		@NamedQuery(name = "ProductBO.updateProductAddInfo", query = "update ProductBO p set p.additionalInfo = :additionalInfo WHERE p.productId = :productId"),
		@NamedQuery(name = "ProductBO.updateProductAuctionDate", query = "update ProductBO p set p.auctionDate = :auctionDate WHERE p.productId = :productId")
})
public class ProductBO {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "item_id")
	private Integer productId;
	
	@Column(name = "item_name")
	private String productName;
	
	@Column(name = "description")
	private String productDescription;
	
	@Column(name = "created_date")
	private Date createdDate; 
	
	@OneToOne
	@JoinColumn(name = "created_by", referencedColumnName = "user_id")
	private UserBO user;
	
	@Column(name = "auction_date")
	private Timestamp auctionDate;
	
	@OneToOne
	@JoinColumn(name = "artist", referencedColumnName = "artist_id")
	private ArtistBO artist;
	
	@Column(name = "estimate_min")
	private Integer estimatedMin;

	@Column(name = "estimate_max")
	private Integer estimatedMax;
	
	@OneToOne
	@JoinColumn(name = "status", referencedColumnName = "status_id")
	private StatusBO status;
	
	@Column(name = "img_url")
	private String imageUrl;
	
	@Column(name = "img_id")
	private String imageId;
	
	@OneToOne
	@JoinColumn(name = "sub_category", referencedColumnName = "sub_category_id")
	private SubCategoryBO category;
	
	@Column(name = "additional_info")
	private String additionalInfo;
}
