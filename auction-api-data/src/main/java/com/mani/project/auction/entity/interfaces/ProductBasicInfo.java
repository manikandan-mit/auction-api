package com.mani.project.auction.entity.interfaces;

import java.sql.Timestamp;

public interface ProductBasicInfo {

	Integer getProductId();

	String getProductName();

	Timestamp getAuctionDate();

	Integer getEstimatedMin();

	Integer getEstimatedMax();

	String getImageUrl();
	
	ArtistBasicInfo getArtist();
	
	SubCategoryBasicInfo getCategory();
	
	String getAdditionalInfo();
}
