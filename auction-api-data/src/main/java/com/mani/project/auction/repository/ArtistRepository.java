package com.mani.project.auction.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mani.project.auction.entity.ArtistBO;
import com.mani.project.auction.entity.interfaces.ArtistBasicInfo;

@Repository
public interface ArtistRepository extends CrudRepository<ArtistBO, Integer>{
	List<ArtistBO> findAll();
	List<ArtistBasicInfo> findBy();

	@Transactional
	@Modifying
	@Query(value = "update artist s set s.active = :activeStatus where s.artist_id = :artist", nativeQuery = true)
	public void updateActiveStatus(@Param("artist") int artistId, @Param("activeStatus") boolean activeStatus);
	
	List<Object[]> listAllActiveArtist();

}
