package com.mani.project.auction.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.mani.project.auction.entity.RoleBO;
import com.mani.project.auction.entity.UserRole;

public interface RoleRepository extends CrudRepository<RoleBO, Integer>{
	Optional<RoleBO> findByRoleName(UserRole name);
}
