package com.mani.project.auction.entity.interfaces;

import java.sql.Timestamp;

public interface ProductListItem {

	Integer getProductId();

	String getProductName();

	Timestamp getAuctionDate();
}
