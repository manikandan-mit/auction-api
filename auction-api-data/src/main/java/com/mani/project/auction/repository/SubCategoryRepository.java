package com.mani.project.auction.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.mani.project.auction.entity.SubCategoryBO;
import com.mani.project.auction.entity.interfaces.SubCategoryBasicInfo;

public interface SubCategoryRepository extends CrudRepository<SubCategoryBO, Integer> {
	
	@Transactional
	@Modifying
	@Query(value = "update sub_category s set s.active = :activeStatus where s.category_id = :category", nativeQuery = true)
	public void updateActiveStatusOnCategory(@Param("category") int categoryId, @Param("activeStatus") boolean activeStatus);

	@Transactional
	@Modifying
	@Query(value = "update sub_category s set s.active = :activeStatus where s.sub_category_id = :subCategory", nativeQuery = true)
	public void updateActiveStatus(@Param("subCategory") int subCategoryId, @Param("activeStatus") boolean activeStatus);
	
	List<SubCategoryBasicInfo> findBy();
	List<Object[]> listAllActiveSubCategory();
}
