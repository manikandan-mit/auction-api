package com.mani.project.auction.repository;

import java.sql.Timestamp;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mani.project.auction.entity.ProductBO;

@Repository
public interface ProductRepository extends PagingAndSortingRepository<ProductBO, Integer> {

	public Page<ProductBO> findAll();
	public Page<ProductBO> findByCategorySubCategoryIdAndCategoryIsActiveAndStatusStatusId(int categoryId, boolean active, int statusId, Pageable paging);
	public Page<ProductBO> findByArtistArtistIdAndArtistActiveAndStatusStatusId(int artistId, boolean active, int statusId, Pageable paging);
	public Page<ProductBO> findByCategorySubCategoryIdAndCategoryIsActiveAndArtistArtistIdAndArtistActiveAndStatusStatusId(int categoryId, boolean isActive, int artistId, boolean active, int statusId, Pageable paging);
	public Page<ProductBO> findByStatusStatusId(int statusId, Pageable paging);
	
	public Page<ProductBO> findAllProducts(Pageable paging);
	public Page<ProductBO> findProductsByCategory(@Param("category") List<Integer> category, Pageable paging);
	public Page<ProductBO> findProductsByArtist(@Param("artist") List<Integer> artist, Pageable paging);
	public Page<ProductBO> findProductsByArtistAndCategory(@Param("artist") List<Integer> artist, @Param("category") List<Integer> category, Pageable paging);
	public Page<Object[]> listAllProducts(@Param("status") Integer status, Pageable paging);
	
	@Transactional
	@Modifying
	public void updateProductStatus(@Param("productId") List<Integer> productIds, @Param("status") Integer status);
	
	@Transactional
	@Modifying
	public void updateProductName(@Param("productName") String productName, @Param("productId") Integer productId);

	@Transactional
	@Modifying
	public void updateProductDesc(@Param("productDescription") String productDescription, @Param("productId") Integer productId);

	@Transactional
	@Modifying
	public void updateProductEstimate(@Param("estimatedMin") Integer estimatedMin, @Param("estimatedMax") Integer estimatedMax, @Param("productId") Integer productId);

	@Transactional
	@Modifying
	public void updateProductAddInfo(@Param("additionalInfo") String additionalInfo, @Param("productId") Integer productId);

	@Transactional
	@Modifying
	public void updateProductAuctionDate(@Param("auctionDate") Timestamp auctionDate, @Param("productId") Integer productId);
}
