package com.mani.project.auction.entity.interfaces;

public interface SubCategoryBasicInfo {

	Integer getSubCategoryId();

	String getSubCategoryName();

}
