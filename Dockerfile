FROM openjdk:8-jdk-alpine
COPY auction-api-web/target/auction-api.jar app.jar
COPY config config
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/app.jar", "--spring.config.location=file:/config/"]