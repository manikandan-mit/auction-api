package com.mani.project.auction.config;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.google.common.base.Predicate;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.service.SecurityScheme;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@EnableWebMvc
@Profile("swagger")
public class SwaggerConfig {

	@Value("${security.ignoreurls:}")
	private String ignoreUrls;

	@Bean
	public Docket configure() {
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("com.mani.project.auction.controller"))
				.paths(PathSelectors.any())
				.build()
				.apiInfo(api())
				.securitySchemes(Arrays.asList(apiKey()))
				.securityContexts(Collections.singletonList(securityContext()));
	}

	private SecurityScheme apiKey() {
		return new ApiKey("Bearer", "token", "header");
	}

	private SecurityContext securityContext() {
		Predicate<String> pathSelector = s -> !Arrays.asList(ignoreUrls.split(",")).contains(s);
		return SecurityContext.builder()
													.securityReferences(defaultAuth())
													.forPaths(pathSelector).build();
	}

	private List<SecurityReference> defaultAuth() {
		final AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
		final AuthorizationScope[] authorizationScopes = new AuthorizationScope[] { authorizationScope };
		return Collections.singletonList(new SecurityReference("Bearer", authorizationScopes));
	}

	private ApiInfo api() {
		return new ApiInfoBuilder().title("Auction Api").description("Api for auction house application")
				.version("1.0.0-SNAPSHOT").contact(new Contact("Manikandan", "", "manikandan.mit@outlook.com")).build();
	}
}
