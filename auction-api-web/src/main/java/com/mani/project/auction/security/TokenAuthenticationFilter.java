package com.mani.project.auction.security;

import java.io.File;
import java.io.IOException;
import java.lang.invoke.MethodHandles;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.web.filter.OncePerRequestFilter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mani.project.auction.dto.UserDetailsDto;
import com.mani.project.auction.entity.UserBO;

public class TokenAuthenticationFilter extends OncePerRequestFilter {

	private AuthenticationEntryPoint authenticationEntryPoint;
	private AuthenticationContext authenticationContext;
	private SecurityUtil securityUtil;

	private static Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	public TokenAuthenticationFilter(AuthenticationEntryPoint authenticationEntryPoint,
			AuthenticationContext authenticationContext, SecurityUtil securityUtil) {
		this.authenticationEntryPoint = authenticationEntryPoint;
		this.authenticationContext = authenticationContext;
		this.securityUtil = securityUtil;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {

		if (authenticationContext.isDisabled()) {
			logger.debug("Authentication disabled. Entering mock mode..");
			ObjectMapper obj = new ObjectMapper();
			String file = ClassLoader.getSystemClassLoader().getResource("mockUser.json").getFile();
			UserBO userDetails = obj.readValue(new File(file), UserBO.class);			
			logger.debug("Populate security context with mock user");
			authenticationContext.setSecurityContextHolder(UserDetailsDto.build(userDetails), null);
			filterChain.doFilter(request, response);
		} else {
			try {
				String token = securityUtil.parseToken(request);
				if (StringUtils.isBlank(token)) {
					logger.error("Token not found");
					throw new AuthenticationCredentialsNotFoundException("Authentication Failed: Token not found");
				}
				doAuthentication(token);
				filterChain.doFilter(request, response);
			} catch (AuthenticationException ex)  {
				logger.error("Authentication Exception");
				authenticationEntryPoint.commence(request, response, ex);
			}
		}
	}

	private void doAuthentication(String token) {
		boolean isValidToken = securityUtil.validateToken(token);
		if (!isValidToken) {
			logger.error("Token validation failed");
			return;
		}
		UserDetailsDto userDetails = securityUtil.getUserFromToken(token);
		authenticationContext.setSecurityContextHolder(userDetails, token);
	}
}
