package com.mani.project.auction.security;

import java.lang.invoke.MethodHandles;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.mani.project.auction.dto.UserDetailsDto;

@Component
public class AuthenticationContext {

	private Environment environment;
	
	@Autowired
	private SecurityUtil securityUtil;
	
	private static Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	public AuthenticationContext(Environment environment) {
		this.environment = environment;
	}
	
	public boolean isEnabled() {
		return Arrays.asList(environment.getActiveProfiles()).contains("security");
	}
	
	public boolean isDisabled() {
		return !isEnabled();
	}
	
	public void setSecurityContextHolder(UserDetailsDto userDetails, String token) {
		logger.debug("Set user in security contest");
		Map<String, Object> additionalInformation = new HashMap<>();
		AuthenticatedUser user = new AuthenticatedUser(userDetails, token, additionalInformation);

		AbstractAuthenticationToken principal = new TokenBasedAuthenticationToken(user);
		SecurityContextHolder.setStrategyName(SecurityContextHolder.MODE_INHERITABLETHREADLOCAL);
		SecurityContextHolder.getContext().setAuthentication(principal);
	}
	
	public String getUserIdFromContext() {
		UserDetailsDto userDetails = securityUtil.getUserFromContext();
		return userDetails.getUserId();
	}
}
