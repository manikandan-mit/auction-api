package com.mani.project.auction.aspect;

import org.aspectj.lang.annotation.Pointcut;

public class PointcutConfig {
	
	@Pointcut("execution(* com.mani.project.auction.controller.*.*(..))")
	public void controllerPointcut() {
		// Pointcut declaration. No implementation needed
	}

	@Pointcut("execution(* com.mani.project.auction.repository.*.*(..))")
	public void repositoryPointcut() {
		// Pointcut declaration. No implementation needed
	}

}
