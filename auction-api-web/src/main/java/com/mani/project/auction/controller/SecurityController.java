package com.mani.project.auction.controller;

import java.lang.invoke.MethodHandles;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mani.project.auction.config.LogSanitizer;
import com.mani.project.auction.dto.UserDetailsDto;
import com.mani.project.auction.entity.RoleBO;
import com.mani.project.auction.entity.UserBO;
import com.mani.project.auction.entity.UserRole;
import com.mani.project.auction.exception.ApplicationError;
import com.mani.project.auction.exception.ErrorCode;
import com.mani.project.auction.exception.ErrorInfo;
import com.mani.project.auction.repository.RoleRepository;
import com.mani.project.auction.repository.UserRepository;
import com.mani.project.auction.security.SecurityUtil;
import com.mani.project.auction.security.entity.LoginRequest;
import com.mani.project.auction.security.entity.SignupRequest;
import com.mani.project.auction.security.entity.TokenResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/security")
@Api("/security")
public class SecurityController {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PasswordEncoder encoder;

	@Autowired
	private SecurityUtil securityUtil;

	private static Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	@PostMapping("/login")
	@ErrorInfo(errorcode = ErrorCode.LOGIN_ERROR)
	@ApiOperation("Login using valid credentials")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "User credentials validated successfully", response = TokenResponse.class),
			@ApiResponse(code = 500, message = "Something went wrong", response = ApplicationError.class) })
	public TokenResponse login(@RequestBody LoginRequest loginRequest) {
		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
		String token = securityUtil.generateToken((UserDetailsDto) authentication.getPrincipal());
		return new TokenResponse(token);
	}

	@GetMapping("/username/{username}")
	@ErrorInfo(errorcode = ErrorCode.USERNAME_ERROR)
	@ApiOperation("Verify if a username is already used")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Username availability verified successfully", response = boolean.class),
			@ApiResponse(code = 500, message = "Something went wrong", response = ApplicationError.class) })
	public boolean verifyUsernameExists(@PathVariable("username") String username) {
		if (userRepository.existsByUsername(username)) {
			logger.debug("Username {} already exists", username);
			return true;
		}
		return false;
	}

	@GetMapping("/email/{email}")
	@ErrorInfo(errorcode = ErrorCode.EMAIL_ERROR)
	@ApiOperation("Verify if a email id is already registered")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Email availability verified successfully", response = boolean.class),
			@ApiResponse(code = 500, message = "Something went wrong", response = ApplicationError.class) })
	public boolean verifyEmailExists(@PathVariable("email") String email) {
		if (userRepository.existsByEmail(email)) {
			logger.debug("Email {} is already registered", email);
			return true;
		}
		return false;
	}

	@GetMapping("/phone/{phone}")
	@ErrorInfo(errorcode = ErrorCode.PHONE_ERROR)
	@ApiOperation("Verify if a phone number is already registered")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Phone number availability verified successfully", response = boolean.class),
			@ApiResponse(code = 500, message = "Something went wrong", response = ApplicationError.class) })
	public boolean verifyPhonenumberExists(@PathVariable("phone") String phone) {
		if (userRepository.existsByPhone(phone)) {
			logger.debug("Phonenumber {} is already registered", phone);
			return true;
		}
		return false;
	}

	@PostMapping("/signup")
	@ErrorInfo(errorcode = ErrorCode.SIGNUP_ERROR)
	@ApiOperation("Signup as a new user")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "New user registered successfully", response = TokenResponse.class),
			@ApiResponse(code = 500, message = "Something went wrong", response = ApplicationError.class) })
	public TokenResponse signup(@RequestBody SignupRequest signupRequest) throws Exception {

		if (userRepository.existsByUsername(signupRequest.getUsername())) {
			logger.error("Username {} already exists", LogSanitizer.sanitize(signupRequest.getUsername()));
			throw new IllegalArgumentException("Error: Username already exists");
		}

		if (userRepository.existsByEmail(signupRequest.getEmail())) {
			logger.error("Email {} is already registered", LogSanitizer.sanitize(signupRequest.getEmail()));
			throw new IllegalArgumentException("Error: Email is already registered");
		}

		if (userRepository.existsByPhone(signupRequest.getPhone())) {
			logger.error("Phone number {} is already registered", LogSanitizer.sanitize(signupRequest.getPhone()));
			throw new IllegalArgumentException("Error: Phone number is already registered");
		}

		UserBO user = new UserBO();

		user.setUserId(securityUtil.generateUserId());
		user.setUsername(signupRequest.getUsername());
		user.setPassword(encoder.encode(signupRequest.getPassword()));
		user.setFirstName(signupRequest.getFirstName());
		user.setLastName(signupRequest.getLastName());
		user.setEmail(signupRequest.getEmail());
		user.setPhone(signupRequest.getPhone());
		user.setIsActive(true);
		Set<RoleBO> userRoles = new HashSet<>();
		if (signupRequest.getRoles().isEmpty()) {
			userRoles.add(roleRepository.findByRoleName(UserRole.USER).orElseThrow(Exception::new));
		} else {
			signupRequest.getRoles().forEach(role -> {
				switch (role) {
				case "admin":
					userRoles.add(roleRepository.findByRoleName(UserRole.ADMIN).get());
					break;
				case "user":
					userRoles.add(roleRepository.findByRoleName(UserRole.USER).get());
					break;
				default:
					userRoles.add(roleRepository.findByRoleName(UserRole.OTHERS).get());
					break;
				}
			});
		}
		user.setRoles(userRoles);
		UserBO userBO = userRepository.save(user);
		UserDetailsDto userprincipal = UserDetailsDto.build(userBO);
		String token = securityUtil.generateToken(userprincipal);
		return new TokenResponse(token);
	}

}
