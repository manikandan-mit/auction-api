package com.mani.project.auction.config;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.mani.project.auction.security.AuthenticationContext;
import com.mani.project.auction.security.UserIdResolver;

@Configuration
public class WebConfig implements WebMvcConfigurer {

	@Autowired
	private AuthenticationContext authenticationContext;

	@Override
	public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
		resolvers.add(new UserIdResolver(authenticationContext));
	}
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
	    registry.addResourceHandler("/swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
	    registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
	}
	
//    @Bean
//    Jackson2ObjectMapperBuilder jackson2ObjectMapperBuilder() {
//        return new Jackson2ObjectMapperBuilder()
//                .featuresToDisable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
//                .featuresToEnable(DeserializationFeature.UNWRAP_ROOT_VALUE, SerializationFeature.WRAP_ROOT_VALUE)
//                .findModulesViaServiceLoader(true);
//    }
}
