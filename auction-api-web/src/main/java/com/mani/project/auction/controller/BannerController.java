package com.mani.project.auction.controller;

import java.io.File;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.mani.project.auction.dto.BannerDto;
import com.mani.project.auction.exception.ApplicationError;
import com.mani.project.auction.exception.ErrorCode;
import com.mani.project.auction.exception.ErrorInfo;
import com.mani.project.auction.service.BannerService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/banners")
@Api(value = "/banners")
public class BannerController {

	private static final String PATH_DELIMITER = "/";

	@Autowired
	private BannerService bannerService;

	private static Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	@GetMapping("/public/get")
	@ErrorInfo(errorcode = ErrorCode.BNR_URL)
	@ApiOperation("Get urls of all banners")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successfully fetched all banner urls", response = String.class, responseContainer = "List"),
			@ApiResponse(code = 500, message = "Something went wrong", response = ApplicationError.class)
	})
	public List<String> getBannerUrls() {
		return bannerService.getBannerUrls();
	}

	@GetMapping("/get")
	@PreAuthorize("hasAuthority('ADMIN')")
	@ErrorInfo(errorcode = ErrorCode.BNR_INFO)
	@ApiOperation("Get all information about banners")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successfully fetched all banner information", response = BannerDto.class, responseContainer = "List"),
			@ApiResponse(code = 500, message = "Something went wrong", response = ApplicationError.class)
	})
	public List<BannerDto> getAllBanners() {
		return bannerService.getAllBanners();
	}
	
	@DeleteMapping("/{bannerId}/{publicId}")
	@PreAuthorize("hasAuthority('ADMIN')")
	@ErrorInfo(errorcode = ErrorCode.BNR_DLT)
	@ApiOperation("Delete a banner")
	@ApiResponses({
		@ApiResponse(code = 200, message = "Banner deleted successfully"),
		@ApiResponse(code = 500, message = "Something went wrong", response = ApplicationError.class)
	})
	public void deleteBanner(@ApiParam(name = "bannerId", value = "Unique id of the banner to be deleted") @PathVariable("bannerId") int bannerId,
														@ApiParam(name = "publicId", value = "Cloudinary id of the banner to be deleted") @PathVariable("publicId") String publicId) throws IOException {
		bannerService.deleteBanner(bannerId, publicId);
	}

	@PostMapping("/upload")
	@PreAuthorize("hasAuthority('ADMIN')")
	@ErrorInfo(errorcode = ErrorCode.BNR_ADD)
	@ApiOperation("Upload a banner")
	@ApiResponses({
		@ApiResponse(code = 200, message = "Banner Uploaded successfully"),
		@ApiResponse(code = 500, message = "Something went wrong", response = ApplicationError.class)
	})
	public void uploadFile(@ApiParam(name = "file", value = "The banner image to be uploaded") @RequestParam("file") MultipartFile file) throws IOException {
		File convFile = new File(System.getProperty("java.io.tmpdir") + PATH_DELIMITER + file.getOriginalFilename());
		try {
			file.transferTo(convFile);
		} catch (IllegalStateException | IOException e) {
			logger.error("File upload failed");
			throw e;
		}
		bannerService.uploadBanner(convFile);
	}
}
