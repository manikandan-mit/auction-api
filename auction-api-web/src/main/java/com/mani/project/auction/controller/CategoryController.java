package com.mani.project.auction.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.mani.project.auction.dto.CategoryDto;
import com.mani.project.auction.dto.SubCategoryDto;
import com.mani.project.auction.entity.StatusUpdateRequest;
import com.mani.project.auction.exception.ApplicationError;
import com.mani.project.auction.exception.ErrorCode;
import com.mani.project.auction.exception.ErrorInfo;
import com.mani.project.auction.service.CategoryService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/categories")
@Api(value = "/categories")
public class CategoryController {
	
	@Autowired
	private CategoryService categoryService;
	
	@GetMapping("/public/get")
	@ErrorInfo(errorcode = ErrorCode.CTGRY_GET)
	@ApiOperation(value = "Get all active categorie details")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Sucessfully fetched categories", response = CategoryDto.class, responseContainer = "List"),
			@ApiResponse(code = 500, message = "Something went wrong", response = ApplicationError.class)
	})
	public List<CategoryDto> getAllActiveCategories() {
		return categoryService.findAllCategories(false);
    }

	@GetMapping("/get")
	@ErrorInfo(errorcode = ErrorCode.CTGRY_GET)
	@PreAuthorize("hasAuthority('ADMIN')")
	@ApiOperation(value = "Get all categorie details")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Sucessfully fetched categories", response = CategoryDto.class, responseContainer = "List"),
			@ApiResponse(code = 500, message = "Something went wrong", response = ApplicationError.class)
	})
	public List<CategoryDto> getAllCategories() {
		return categoryService.findAllCategories(true);
    }

	@PatchMapping("/update/category")
	@ErrorInfo(errorcode = ErrorCode.CTGRY_UPDT)
	@PreAuthorize("hasAuthority('ADMIN')")
	@ApiOperation(value = "Update categorie status")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Sucessfully updated category status"),
			@ApiResponse(code = 500, message = "Something went wrong", response = ApplicationError.class)
	})
	public void updateCategoryStatus(@RequestBody StatusUpdateRequest statusUpdateRequest) {
		categoryService.updateActiveStatusOfCategories(Integer.parseInt(statusUpdateRequest.getId()), statusUpdateRequest.isActiveStatus());
    }

	@PatchMapping("/update/subcategory")
	@ErrorInfo(errorcode = ErrorCode.SUB_CTGRY_UPDT)
	@PreAuthorize("hasAuthority('ADMIN')")
	@ApiOperation(value = "Update sub categorie status")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Sucessfully updated sub category status"),
			@ApiResponse(code = 500, message = "Something went wrong", response = ApplicationError.class)
	})
	public void updateSubCategoryStatus(@RequestBody StatusUpdateRequest statusUpdateRequest) {
		categoryService.updateActiveStatusOfSubCategories(Integer.parseInt(statusUpdateRequest.getId()), statusUpdateRequest.isActiveStatus());
    }
	
	@PostMapping("/add/subcategory")
	@ResponseStatus(HttpStatus.CREATED)
	@ErrorInfo(errorcode = ErrorCode.SUB_CTGRY_ADD)
	@PreAuthorize("hasAuthority('ADMIN')")
	@ApiOperation(value = "Create a new Sub category")
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Sucessfully created new sub category"),
			@ApiResponse(code = 400, message = "Category not found", response = ApplicationError.class),
			@ApiResponse(code = 500, message = "Something went wrong", response = ApplicationError.class)
	})
	public void createSubCategory(@RequestBody SubCategoryDto subCategoryDto) {
		categoryService.addSubCategory(subCategoryDto);
	}
	
	@PostMapping("/add")
	@ResponseStatus(HttpStatus.CREATED)
	@ErrorInfo(errorcode = ErrorCode.CTGRY_ADD)
	@PreAuthorize("hasAuthority('ADMIN')")
	@ApiOperation(value = "Create a new category")
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Sucessfully created new category"),
			@ApiResponse(code = 500, message = "Something went wrong", response = ApplicationError.class)
	})
	public void createCategory(@RequestBody CategoryDto categoryDto) {
		categoryService.addCategory(categoryDto);
	}
	
	@GetMapping("/public/subcategory/list")
	@ErrorInfo(errorcode = ErrorCode.SUB_CTGRY_GET)
	@ApiOperation(value = "Get list of all sub categories")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Sucessfully fetched sub categories", response = SubCategoryDto.class, responseContainer = "List"),
			@ApiResponse(code = 500, message = "Something went wrong", response = ApplicationError.class)
	})
	public List<SubCategoryDto> listAllSubCategory() {
		return categoryService.listAllActiveSubCategory();
	}
	
	
	
}
