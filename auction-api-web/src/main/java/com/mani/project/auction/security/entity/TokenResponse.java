package com.mani.project.auction.security.entity;

public class TokenResponse {
	private String token;

	public TokenResponse(String token) {
		super();
		this.token = token;
	}

	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}	
}
