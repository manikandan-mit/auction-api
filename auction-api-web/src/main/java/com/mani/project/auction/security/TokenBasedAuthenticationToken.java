package com.mani.project.auction.security;

import java.util.Map;

import org.springframework.security.authentication.AbstractAuthenticationToken;

public class TokenBasedAuthenticationToken  extends AbstractAuthenticationToken{

	private static final long serialVersionUID = 1L;

	private transient AuthenticatedUser principal;
	private String securityToken;
	private transient Map<String, Object>  additionalInformation;
	
	public TokenBasedAuthenticationToken(AuthenticatedUser principal) {
		super(principal.getUserDetails().getAuthorities());
		this.principal = principal;
		setAuthenticated(true);
	}

	@Override
	public Object getCredentials() {
		return null;
	}

	@Override
	public Object getPrincipal() {
		return this.principal;
	}
	
	public String getUsername(){
		return principal != null  ?  principal.getUserDetails().getUsername()  :  null;
	}

	public String getSecurityToken() {
		return securityToken;
	}

	public void setSecurityToken(String securityToken) {
		this.securityToken = securityToken;
	}

	public Map<String, Object> getAdditionalInformation() {
		return additionalInformation;
	}

	public void setAdditionalInformation(Map<String, Object> additionalInformation) {
		this.additionalInformation = additionalInformation;
	}

	public void setPrincipal(AuthenticatedUser principal) {
		this.principal = principal;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((additionalInformation == null) ? 0 : additionalInformation.hashCode());
		result = prime * result + ((principal == null) ? 0 : principal.hashCode());
		result = prime * result + ((securityToken == null) ? 0 : securityToken.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TokenBasedAuthenticationToken other = (TokenBasedAuthenticationToken) obj;
		if (additionalInformation == null) {
			if (other.additionalInformation != null)
				return false;
		} else if (!additionalInformation.equals(other.additionalInformation))
			return false;
		if (principal == null) {
			if (other.principal != null)
				return false;
		} else if (!principal.equals(other.principal))
			return false;
		if (securityToken == null) {
			if (other.securityToken != null)
				return false;
		} else if (!securityToken.equals(other.securityToken))
			return false;
		return true;
	}

	
	
}
