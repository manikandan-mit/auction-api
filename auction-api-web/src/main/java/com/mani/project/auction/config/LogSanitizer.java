package com.mani.project.auction.config;

public class LogSanitizer {

	private static int maxInputLength = 25;
	
	public static String sanitize(String input) {
		
		if(input == null)
			return input;
		
		input = input.replaceAll("[\n|\r|\t]", input);
		
		if(input.length() < maxInputLength)
			return input;
		
		return input.substring(0, maxInputLength);
	}
}
