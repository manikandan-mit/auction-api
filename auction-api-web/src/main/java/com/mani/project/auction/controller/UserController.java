package com.mani.project.auction.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mani.project.auction.dto.UserDto;
import com.mani.project.auction.entity.StatusUpdateRequest;
import com.mani.project.auction.exception.ApplicationError;
import com.mani.project.auction.exception.ErrorCode;
import com.mani.project.auction.exception.ErrorInfo;
import com.mani.project.auction.security.UserIdParameter;
import com.mani.project.auction.service.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/user")
@Api("/user")
public class UserController {

	@Autowired
	private UserService userService;
	
	@GetMapping("/me")
	@PreAuthorize("hasAuthority('ADMIN') or hasAuthority('USER')")
	@ErrorInfo(errorcode = ErrorCode.USR_ERROR)
	@ApiOperation("Get detils about a user")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "User details fetched successfully", response = UserDto.class),
			@ApiResponse(code = 400, message = "User not found", response = ApplicationError.class),
			@ApiResponse(code = 500, message = "Something went wrong", response = ApplicationError.class)
	})
	public UserDto getUserDetails(UserIdParameter userId) {
		return userService.findUserById(userId.getId());
	}

	@GetMapping("/{userId}")
	@PreAuthorize("hasAuthority('ADMIN')")
	@ErrorInfo(errorcode = ErrorCode.USR_ERROR)
	@ApiOperation("Get detils about a user")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "User details fetched successfully", response = UserDto.class),
			@ApiResponse(code = 400, message = "User not found", response = ApplicationError.class),
			@ApiResponse(code = 500, message = "Something went wrong", response = ApplicationError.class)
	})
	public UserDto getUserDetails(@ApiParam(name = "userId", value = "User id of the user to be fetched") @PathVariable("userId") String userId) {
		return userService.findUserById(userId);
	}

	@GetMapping("/get")
	@PreAuthorize("hasAuthority('ADMIN')")
	@ErrorInfo(errorcode = ErrorCode.USR_GET)
	@ApiOperation("Get list of all users")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "User list fetched successfully", response = UserDto.class, responseContainer = "List"),
			@ApiResponse(code = 500, message = "Something went wrong", response = ApplicationError.class)
	})
	public List<UserDto> getAllCustomerDetails(){
		return userService.getusers();
	}
	
	@PatchMapping("/update/status")
	@PreAuthorize("hasAuthority('ADMIN')")
	@ErrorInfo(errorcode = ErrorCode.USR_STATUS_UPDT)
	@ApiOperation("Activate or Deactivate a user")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "User status updated successfully"),
			@ApiResponse(code = 500, message = "Something went wrong", response = ApplicationError.class)
	})
	public void updateUserStatus(@RequestBody StatusUpdateRequest statusUpdateRequest) {
		userService.updateActiveStatusOfUser(statusUpdateRequest.getId(), statusUpdateRequest.isActiveStatus());
	}
	
}
