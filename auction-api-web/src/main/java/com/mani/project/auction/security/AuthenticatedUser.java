package com.mani.project.auction.security;

import java.util.Map;

import com.mani.project.auction.dto.UserDetailsDto;

public class AuthenticatedUser {
	
	private UserDetailsDto userDetails;
	private String securityToken;
	private Map<String, Object> additionalInformation;
	
	public AuthenticatedUser(UserDetailsDto userDetails, String securityToken,
			Map<String, Object> additionalInformation) {
		this.userDetails = userDetails;
		this.securityToken = securityToken;
		this.additionalInformation = additionalInformation;
	}

	public UserDetailsDto getUserDetails() {
		return userDetails;
	}
	public void setUserDetails(UserDetailsDto userDetails) {
		this.userDetails = userDetails;
	}
	public String getSecurityToken() {
		return securityToken;
	}
	public void setSecurityToken(String securityToken) {
		this.securityToken = securityToken;
	}
	public Map<String, Object> getAdditionalInformation() {
		return additionalInformation;
	}
	public void setAdditionalInformation(Map<String, Object> additionalInformation) {
		this.additionalInformation = additionalInformation;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((additionalInformation == null) ? 0 : additionalInformation.hashCode());
		result = prime * result + ((securityToken == null) ? 0 : securityToken.hashCode());
		result = prime * result + ((userDetails == null) ? 0 : userDetails.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AuthenticatedUser other = (AuthenticatedUser) obj;
		if (additionalInformation == null) {
			if (other.additionalInformation != null)
				return false;
		} else if (!additionalInformation.equals(other.additionalInformation))
			return false;
		if (securityToken == null) {
			if (other.securityToken != null)
				return false;
		} else if (!securityToken.equals(other.securityToken))
			return false;
		if (userDetails == null) {
			if (other.userDetails != null)
				return false;
		} else if (!userDetails.equals(other.userDetails))
			return false;
		return true;
	}
	
	

}
