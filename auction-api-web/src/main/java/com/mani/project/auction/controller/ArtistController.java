package com.mani.project.auction.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.mani.project.auction.dto.ArtistDto;
import com.mani.project.auction.entity.StatusUpdateRequest;
import com.mani.project.auction.exception.ApplicationError;
import com.mani.project.auction.exception.ErrorCode;
import com.mani.project.auction.exception.ErrorInfo;
import com.mani.project.auction.service.ArtistService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(value = "/artist")
@Api(value = "/artist")
public class ArtistController {

	@Autowired
	private ArtistService artistService;
	
	@GetMapping("/public/get")
	@ErrorInfo(errorcode = ErrorCode.ARTIST_GET_ALL)
	@ApiOperation(value = "Get list of all active artists")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Artists information retrived successfully", response = ArtistDto.class, responseContainer = "List"),
			@ApiResponse(code = 500, message = "Something went wrong", response = ApplicationError.class)
	})
	public List<ArtistDto> getAllActiveArtists(){
		return artistService.getAllArtists(false);
	}

	@GetMapping("/get")
	@ErrorInfo(errorcode = ErrorCode.ARTIST_GET_ALL)
	@PreAuthorize("hasAuthority('ADMIN')")
	@ApiOperation(value = "Get list of all artists")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Artists information retrived successfully", response = ArtistDto.class, responseContainer = "List"),
			@ApiResponse(code = 500, message = "Something went wrong", response = ApplicationError.class)
	})
	public List<ArtistDto> getAllArtists(){
		return artistService.getAllArtists(true);
	}

	@GetMapping("/public/list")
	@ErrorInfo(errorcode = ErrorCode.ARTIST_GET_ALL)
	@ApiOperation(value = "Get list of all artists")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Artists information retrived successfully", response = ArtistDto.class, responseContainer = "List"),
			@ApiResponse(code = 500, message = "Something went wrong", response = ApplicationError.class)
	})
	public List<ArtistDto> listAllArtists(){
		return artistService.listAllActiveArtist();
	}

	@GetMapping("/public/get/{artistId}")
	@ErrorInfo(errorcode = ErrorCode.ARTIST_GET)
	@ApiOperation(value = "Get details of artist based on id")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Artist information retrived successfully", response = ArtistDto.class),
			@ApiResponse(code = 400, message = "Artist not found", response = ApplicationError.class),
			@ApiResponse(code = 500, message = "Something went wrong", response = ApplicationError.class)
	})
	public ArtistDto getArtist(@ApiParam(name = "artistId", value = "Id of the artist") @PathVariable("artistId") String artistId,
													 @ApiParam(value = "Populate default profile image", defaultValue = "true") @RequestParam(name = "populateDefaultProfile", defaultValue = "true") boolean populateDefaultProfile) {
		return artistService.getArtistById(artistId, populateDefaultProfile);
	}
	
	@PatchMapping("/update")
	@ErrorInfo(errorcode = ErrorCode.ARTIST_UPDT)
	@PreAuthorize("hasAuthority('ADMIN')")
	@ApiOperation(value = "Update artist status")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Sucessfully updated artist status"),
			@ApiResponse(code = 500, message = "Something went wrong", response = ApplicationError.class)
	})
	public void updateArtistStatus(@RequestBody StatusUpdateRequest statusUpdateRequest) {
		artistService.updateActiveStatusOfArtist(Integer.parseInt(statusUpdateRequest.getId()), statusUpdateRequest.isActiveStatus());
    }

	@PostMapping("/info/save")
	@ErrorInfo(errorcode = ErrorCode.ARTIST_SAVE)
	@PreAuthorize("hasAuthority('ADMIN')")
	@ApiOperation(value = "Save artist info")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Sucessfully saved artist information"),
			@ApiResponse(code = 500, message = "Something went wrong", response = ApplicationError.class)
	})
	public void saveArtistInfo(@RequestBody ArtistDto artistInfo,
														@ApiParam(value = "Image id of the image to be deleted") @RequestParam(required = false, name = "deleteId") String deleteId) throws Exception {
		artistService.saveArtist(null, artistInfo, deleteId);
	}
	
	@PostMapping("/save")
	@ErrorInfo(errorcode = ErrorCode.ARTIST_SAVE)
	@PreAuthorize("hasAuthority('ADMIN')")
	@ApiOperation(value = "Save artist info")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Sucessfully saved artist information"),
			@ApiResponse(code = 500, message = "Something went wrong", response = ApplicationError.class)
	})
	public void saveArtist(@RequestPart("file") MultipartFile file,
												@RequestPart("artistInfo") ArtistDto artistInfo,
												@ApiParam(value = "Image id of the image to be deleted") @RequestParam(required = false, name = "deleteId") String deleteId) throws Exception {
		artistService.saveArtist(file, artistInfo, deleteId);
	}

}
