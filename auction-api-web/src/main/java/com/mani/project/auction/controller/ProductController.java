package com.mani.project.auction.controller;

import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.mani.project.auction.dto.CreateProductRequestDto;
import com.mani.project.auction.dto.ProductDto;
import com.mani.project.auction.dto.ProductResponseDto;
import com.mani.project.auction.exception.ApplicationError;
import com.mani.project.auction.exception.ErrorCode;
import com.mani.project.auction.exception.ErrorInfo;
import com.mani.project.auction.security.UserIdParameter;
import com.mani.project.auction.service.ProductService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(value = "/products")
@Api(value = "/products")
public class ProductController {

	@Autowired
	private ProductService productService;
	
	@GetMapping("/public/get")
	@ErrorInfo(errorcode = ErrorCode.PRDT_GET_ALL)
	@ApiOperation(value = "Get all Products")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Sucessfully fetched categories", response = ProductResponseDto.class),
			@ApiResponse(code = 500, message = "Something went wrong", response = ApplicationError.class)
	})
	public ProductResponseDto getAllProducts(@ApiParam(value = "Category id") @RequestParam(name = "categoryId", required = false) List<Integer> categoryId,
																					@ApiParam(value = "Artist id") @RequestParam(name = "artistId", required = false) List<Integer> artistId,
																					@ApiParam(value = "Page number", defaultValue = "1") @RequestParam(name = "pageNo", defaultValue = "1") int pageNo,
																					@ApiParam(value = "Page size", defaultValue = "5") @RequestParam(name = "pageSize", defaultValue = "5") int pageSize,
																					@ApiParam(value = "Get list of products only", defaultValue = "false") @RequestParam(name="getListOnly", defaultValue = "false") boolean getListOnly,
																					@ApiParam(value = "Status of the products", defaultValue = "1") @RequestParam(name="status", defaultValue = "1") Integer status) {
		ProductResponseDto result = null;
		if(getListOnly) {
			result = productService.listAllProducts(pageNo, pageSize, status);
		} else {
			result = productService.findAllProducts(categoryId, artistId, pageNo, pageSize);
		}
		return result;
	}
	
	@GetMapping("/public/get/{productId}")
	@ErrorInfo(errorcode = ErrorCode.PRDT_GET)
	@ApiOperation(value = "Get Products specified by the id")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Sucessfully fetched categories", response = ProductDto.class),
			@ApiResponse(code = 500, message = "Something went wrong", response = ApplicationError.class)
	})
	public ProductDto getProductById(@ApiParam(name = "productId", value = "Id of the product") @PathVariable("productId") int productId) {
		return productService.findProductById(productId);
	}
	
	@GetMapping("/update/{productIds}/{status}")
	@ErrorInfo(errorcode = ErrorCode.PRDT_STATUS_UPDT)
	@PreAuthorize("hasAuthority('ADMIN')")
	@ApiOperation(value = "Update product status")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Product status sucessfully updated"),
			@ApiResponse(code = 500, message = "Something went wrong", response = ApplicationError.class)
	})
	public void updateProductStatus(@PathVariable("productIds") List<Integer> productIds, @PathVariable("status") Integer status) {
		productService.updateProductStatus(productIds, status);
	}
	
	@PostMapping("/add")
	@ErrorInfo(errorcode = ErrorCode.PRDT_ADD)
	@PreAuthorize("hasAuthority('ADMIN')")
	@ResponseStatus(HttpStatus.CREATED)
	@ApiOperation(value = "Add new product")
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Product added successfully"),
			@ApiResponse(code = 500, message = "Something went wrong", response = ApplicationError.class)
	})
	public void createNewProduct(@RequestPart("file") MultipartFile file,  @RequestPart("productInfo") CreateProductRequestDto product, UserIdParameter userId) throws Exception {
		productService.saveProduct(file, product, userId.getId());
	}
	
	@GetMapping("/update")
	@ErrorInfo(errorcode = ErrorCode.PRDT_UPDT)
	@PreAuthorize("hasAuthority('ADMIN')")
	@ApiOperation(value = "Product information updated")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Product info updated successfully"),
			@ApiResponse(code = 500, message = "Something went wrong", response = ApplicationError.class)
	})
	public void updateProductInfo(@ApiParam(value = "Field Name") @RequestParam("fieldName") String fieldName,
																@ApiParam(value = "Field Value") @RequestParam("fieldValue") String fieldValue,
																@ApiParam(value = "Product id") @RequestParam("productId") Integer productId) throws ParseException {
		if(fieldName.equalsIgnoreCase("name")) {
			productService.updateProductName(fieldValue, productId);
		} else if(fieldName.equalsIgnoreCase("desc")) {
			productService.updateProductDesc(fieldValue, productId);
		} else if(fieldName.equalsIgnoreCase("estimate")) {
			productService.updateProductEstimate(Integer.parseInt(fieldValue.split("-")[0]), Integer.parseInt(fieldValue.split("-")[1]), productId);
		} else if(fieldName.equalsIgnoreCase("additionalInfo")) {
			productService.updateProductAddInfo(fieldValue, productId);
		} else if(fieldName.equalsIgnoreCase("auctionDate")) {
			productService.updateProductAuctionDate(fieldValue, productId);
		}
	}
}
