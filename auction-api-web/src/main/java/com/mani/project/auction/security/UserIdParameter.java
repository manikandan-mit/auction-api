package com.mani.project.auction.security;

public class UserIdParameter {
	
	private String id;

	public UserIdParameter(String id) {
		super();
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	
}
