package com.mani.project.auction.security;

import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

public class UserIdResolver implements HandlerMethodArgumentResolver {

	private AuthenticationContext authenticationContext;
	
	public UserIdResolver(AuthenticationContext authenticationContext) {
		this.authenticationContext = authenticationContext;
	}

	@Override
	public boolean supportsParameter(MethodParameter parameter) {
		return parameter.getParameterType().equals(UserIdParameter.class);
	}

	@Override
	public UserIdParameter resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer,
			NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
		String userId = this.authenticationContext.getUserIdFromContext();
		return new UserIdParameter(userId);
	}

}
