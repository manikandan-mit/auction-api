package com.mani.project.auction.exception;

import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler{

	@ExceptionHandler(IllegalArgumentException.class)
	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ApplicationError handleAuthenticationException(IllegalArgumentException ex, HandlerMethod handler) {
		ErrorCode errorCode = getErrorCode(handler);
		return new ApplicationError(errorCode.getCode(), ex.getMessage());
	}
	
	@ExceptionHandler(UsernameNotFoundException.class)
	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ApplicationError handleUsernameNotFoundException(UsernameNotFoundException ex, HandlerMethod handler) {
		ErrorCode errorCode = getErrorCode(handler);
		return new ApplicationError(errorCode.getCode(), ex.getMessage());
	}

	@ExceptionHandler(ResourceNotFoundException.class)
	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ApplicationError handleResourceNotFoundException(ResourceNotFoundException ex, HandlerMethod handler) {
		ErrorCode errorCode = getErrorCode(handler);
		return new ApplicationError(errorCode.getCode(), ex.getMessage());
	}

	@ExceptionHandler(AuthenticationCredentialsNotFoundException.class)
	@ResponseStatus(code = HttpStatus.UNAUTHORIZED)
	public ApplicationError handleAuthenticationCredentialsNotFoundException(AuthenticationCredentialsNotFoundException ex, HandlerMethod handler) {
		ErrorCode errorCode = getErrorCode(handler);
		return new ApplicationError(errorCode.getCode(), ex.getMessage());
	}
	
	@ExceptionHandler(AccessDeniedException.class)
	@ResponseStatus(code = HttpStatus.FORBIDDEN)
	public ApplicationError handleAccessDeniedException(AccessDeniedException ex, HandlerMethod handler) {
		ErrorCode errorCode = getErrorCode(handler);
		return new ApplicationError(errorCode.getCode(), ex.getMessage());
	}

	@ExceptionHandler(Exception.class)
	@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	public ApplicationError handleException(Exception ex, HandlerMethod handler) {
//		ex.printStackTrace();
		ErrorCode errorCode = getErrorCode(handler);
		return new ApplicationError(errorCode.getCode(), "Something went wrong..");
	}
	
	private ErrorCode getErrorCode(HandlerMethod handler) {
		Optional<ErrorInfo> errorInfo  = Optional.ofNullable(handler.getMethod().getAnnotation(ErrorInfo.class));
		return errorInfo.map(ErrorInfo::errorcode).orElse(ErrorCode.DEFAULT);
	}
}
