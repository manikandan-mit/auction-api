# Auction API

Api for an Auction house application.

## Requirements

For building and running the application you need:

- [JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven 3](https://maven.apache.org)

## Running the application locally

There are several ways to run a Spring Boot application on your local machine. One way is to execute the `main` method in the `com.mani.project.auction.AuctionApplication` class from your IDE.

Alternatively you can use the [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html) like so:

```shell
mvn spring-boot:run
```

Please make sure to add config folder location to your classpath.

To run the project without Spring Boot plugin, go to the project root folder and use the following command:

```shell
java -jar auction-api-web/target/auction-api.jar --spring.config.location=file:./config/
```

## Cloudinary
This application user [Cloudinary](https://cloudinary.com/users/login) to store images. Create a account in cloudinary and update the following proeprties in application.properties file.

* cloudinary.cloud.name
* cloudinary.api.key
* cloudinary.api.secret

## Database
You can use any SQL database of your choice. Update the application.properties files accordingly. Use `auction-house.sql` file attached to import table structures. To view the queries during execution add `query` to profiles.

## Swagger
After successfully running the application, you can hit [http://localhost:8080/auction-api/swagger-ui.html](http://localhost:8080/auction-api/swagger-ui.html) to view the swagger file and to try out the end points.

## Docker
To create a docker image you can use the following command:

```shell
docker build -t auction-api .
```

To run the docker image use:

```shell
docker container run --publish 8080:8080 auction-api 
```