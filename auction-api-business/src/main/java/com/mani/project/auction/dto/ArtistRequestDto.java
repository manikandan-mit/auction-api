package com.mani.project.auction.dto;

import org.springframework.web.multipart.MultipartFile;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ArtistRequestDto extends ArtistDto{

	private static final long serialVersionUID = 1L;
	public MultipartFile file;

}
