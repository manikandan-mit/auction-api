package com.mani.project.auction.service;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.mani.project.auction.dto.BannerDto;
import com.mani.project.auction.entity.BannerBO;

public interface BannerService {

	List<String> getBannerUrls();
	List<BannerDto> getAllBanners();
	List<BannerBO> getBannerInfoFromDB();
	void deleteBanner(int bannerId, String publicId) throws IOException;
	void uploadBanner(File file) throws IOException;
	public void clearBannerCache();
}
