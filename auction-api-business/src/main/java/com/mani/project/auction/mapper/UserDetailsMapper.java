package com.mani.project.auction.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.mani.project.auction.dto.UserDto;
import com.mani.project.auction.entity.RoleBO;
import com.mani.project.auction.entity.UserBO;
import com.mani.project.auction.entity.UserRole;

@Component
public class UserDetailsMapper {

	public UserDto mapUserBoToDto(UserBO userBO) {
		UserDto userDto = new UserDto();
		userDto.setUserId(userBO.getUserId());
		userDto.setUsername(userBO.getUsername());
		userDto.setFirstName(userBO.getFirstName());
		userDto.setLastName(userBO.getLastName());
		userDto.setEmail(userBO.getEmail());
		userDto.setPhone(userBO.getPhone());
		userDto.setIsActive(userBO.getIsActive());
		List<String> roles = userBO.getRoles().stream().map(RoleBO::getRoleName).map(UserRole::toString).collect(Collectors.toList());
		userDto.setRoles(roles);
		return userDto;
	}
}
