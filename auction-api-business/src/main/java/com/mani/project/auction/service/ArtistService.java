package com.mani.project.auction.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.mani.project.auction.dto.ArtistDto;

public interface ArtistService {
	public List<ArtistDto> getAllArtistFromDB();
	public List<ArtistDto> listAllActiveArtist();
	public List<ArtistDto> getAllArtists(boolean includeInactive);
	public ArtistDto getArtistById(String artistId, boolean populateDefaultProfile);
	void clearArtistCache();
	void updateActiveStatusOfArtist(int artistId, boolean activeStatus);
	void saveArtist(MultipartFile file, ArtistDto artistInfo, String deleteId) throws Exception;
}
