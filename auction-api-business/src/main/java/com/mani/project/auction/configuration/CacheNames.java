package com.mani.project.auction.configuration;

public class CacheNames {
	
	public static final String CATEGORY_CACHE = "categoryCache";
	public static final String BANNER_CACHE = "bannerCache";
	public static final String ARTIST_CACHE = "'artistCache'";
	private CacheNames() {
		
	}

}
