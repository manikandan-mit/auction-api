package com.mani.project.auction.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ProductResponseDto {
	private List<ProductDto> products;
	private Integer pageNumber;
	private Long totalNumberOfElements;
	private Integer productsPerPage;
}
