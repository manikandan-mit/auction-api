package com.mani.project.auction.service.impl;

import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.mani.project.auction.configuration.CacheNames;
import com.mani.project.auction.dto.CategoryDto;
import com.mani.project.auction.dto.SubCategoryDto;
import com.mani.project.auction.entity.CategoryBO;
import com.mani.project.auction.entity.SubCategoryBO;
import com.mani.project.auction.exception.ResourceNotFoundException;
import com.mani.project.auction.mapper.CategoryMapper;
import com.mani.project.auction.repository.CategoryRepository;
import com.mani.project.auction.repository.SubCategoryRepository;
import com.mani.project.auction.service.CategoryService;

@Service
public class CategoryServiceImpl implements CategoryService{

	private static Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	@Autowired
	private CategoryRepository categoryRepository;
	
	@Autowired
	private SubCategoryRepository subCategoryRepository;
	
	@Autowired
	private CategoryMapper categoryMapper;
	
	@Autowired
	private CategoryService proxyCategoryService;
	
	@Override
	public List<CategoryDto> findAllCategories(boolean includeInactiveCategories) {
		List<CategoryBO> categoryBOList = proxyCategoryService.getCategoryInfoFromDB();
		if(!includeInactiveCategories) {
			categoryBOList = categoryBOList.stream().filter(category -> category.isActive()).collect(Collectors.toList());
		}
		
		return categoryMapper.mapCategoryBoToDto(categoryBOList);
	}

	@Override
	@Cacheable(cacheNames = CacheNames.CATEGORY_CACHE, key = "'category'", unless = "#result == null")
	public List<CategoryBO> getCategoryInfoFromDB() {
		logger.info("Fetching category data from database.");
		return categoryRepository.findAll();
	}
	
	@Override
	@CacheEvict(cacheNames = CacheNames.CATEGORY_CACHE, key = "'category'")
	public void clearCategoryCache() {
		// Cache Evict method. No implementation required
	}

	@Override
	public void updateActiveStatusOfCategories(int categoryId, boolean activeStatus) {
		categoryRepository.updateActiveStatus(categoryId, activeStatus);
		if(!activeStatus)  {
			subCategoryRepository.updateActiveStatusOnCategory(categoryId, activeStatus);			
		}
		proxyCategoryService.clearCategoryCache();
	}

	@Override
	public void updateActiveStatusOfSubCategories(int subCategoryId, boolean activeStatus) {
		subCategoryRepository.updateActiveStatus(subCategoryId, activeStatus);
		proxyCategoryService.clearCategoryCache();
	}

	@Override
	public void addSubCategory(SubCategoryDto subCategoryDto) {
		CategoryBO category = categoryRepository.findById(subCategoryDto.getCategoryId()).orElseThrow(() -> new ResourceNotFoundException("Category Not Found"));
		SubCategoryBO subCategoryBO = categoryMapper.mapSubCategoryDtoToBo(subCategoryDto);
		subCategoryBO.setCategory(category);
		subCategoryRepository.save(subCategoryBO);
		proxyCategoryService.clearCategoryCache();
	}

	@Override
	public void addCategory(CategoryDto categoryDto) {
		CategoryBO categoryBO = categoryMapper.mapCategoryDtoToBo(categoryDto);
		categoryRepository.save(categoryBO);
		proxyCategoryService.clearCategoryCache();		
	}
	
	@Override
	public List<SubCategoryDto> listAllActiveSubCategory() {
		List<Object[]> resultList = subCategoryRepository.listAllActiveSubCategory();
		return resultList.stream().map(result -> {
							SubCategoryDto sub = new SubCategoryDto();
							sub.setSubCategoryId((Integer)result[0]);
							sub.setSubCategoryName((String)result[1]);
							return sub;
						}).collect(Collectors.toList());
	}	

}
