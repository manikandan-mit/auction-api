package com.mani.project.auction.service;

import java.text.ParseException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.mani.project.auction.dto.CreateProductRequestDto;
import com.mani.project.auction.dto.ProductDto;
import com.mani.project.auction.dto.ProductResponseDto;

public interface ProductService {
	ProductResponseDto findAllProducts(List<Integer> categoryId, List<Integer> artistId, int pageNo, int pageSize);
	ProductResponseDto listAllProducts(int pageNo, int pageSize, Integer status);
	ProductDto findProductById(int productId);
	void updateProductStatus(List<Integer> productIds, Integer status);
	void saveProduct(MultipartFile file, CreateProductRequestDto product, String userId) throws Exception;
	void updateProductName(String productName, Integer productId);
	void updateProductDesc(String productDescription, Integer productId);
	void updateProductEstimate(Integer estimatedMin, Integer estimatedMax, Integer productId);
	void updateProductAddInfo(String productAdditionalInfo, Integer productId);
	void updateProductAuctionDate(String auctionDate, Integer productId) throws ParseException;
}
