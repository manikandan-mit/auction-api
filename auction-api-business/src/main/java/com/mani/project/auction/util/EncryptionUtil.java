package com.mani.project.auction.util;

import javax.annotation.PostConstruct;

import org.jasypt.util.text.BasicTextEncryptor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class EncryptionUtil {
	
	@Value("${jasypt.enc.key}")
	private String encKey;
	
	private BasicTextEncryptor basicTextEncryptor;
	
	@PostConstruct
	public void init()  {
		this.basicTextEncryptor = new BasicTextEncryptor();
		this.basicTextEncryptor.setPassword(encKey);
	}
	
	public String encrypt(String input) {
		return basicTextEncryptor.encrypt(input);
	}

	public String decrypt(String input) {
		return basicTextEncryptor.decrypt(input);
	}
}
