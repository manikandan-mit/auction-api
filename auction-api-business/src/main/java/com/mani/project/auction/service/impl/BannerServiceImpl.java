package com.mani.project.auction.service.impl;

import java.io.File;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.mani.project.auction.configuration.CacheNames;
import com.mani.project.auction.dto.BannerDto;
import com.mani.project.auction.entity.BannerBO;
import com.mani.project.auction.mapper.BannerMapper;
import com.mani.project.auction.repository.BannerRepository;
import com.mani.project.auction.service.BannerService;

@Service
public class BannerServiceImpl implements BannerService {

	@Autowired
	private BannerRepository bannerRepository;
	
	@Autowired
	private BannerMapper bannerMapper;
	
	@Autowired
	private Cloudinary cloudinaryService;
	
	@Autowired
	private BannerService proxyBannerService;
	
	private static Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	@Override
	public List<String> getBannerUrls() {
		List<BannerBO> bannerBoList = proxyBannerService.getBannerInfoFromDB();
		return bannerBoList.stream().map(BannerBO::getUrl).collect(Collectors.toList());
	}

	@Override
	public List<BannerDto> getAllBanners() {
		List<BannerBO> bannerBoList = proxyBannerService.getBannerInfoFromDB();
		return bannerMapper.mapBannerBoToDto(bannerBoList);
	}
	
	@Override
	@Cacheable(cacheNames = CacheNames.BANNER_CACHE, key = "'banners'", unless = "#result == null")
	public List<BannerBO> getBannerInfoFromDB(){
		logger.info("Fetching banner data from database");
		return bannerRepository.findAll();
	}

	@Override
	public void deleteBanner(int bannerId, String publicId) throws IOException {
		try {
			cloudinaryService.uploader().destroy(publicId, ObjectUtils.emptyMap());
			bannerRepository.deleteById(bannerId);
			proxyBannerService.clearBannerCache();
		} catch (IOException e) {
			logger.error("Banner deletion failed");
			throw e;
		}
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void uploadBanner(File file) throws IOException {
		Map uploadResult = null;
		try {
			uploadResult = cloudinaryService.uploader().upload(file,
			        ObjectUtils.asMap("resource_type", "auto"));
		} catch (IOException e) {
			logger.error("Banner upload failed");
			throw e;
		}

		if(uploadResult != null) {
			BannerBO bo = new BannerBO();
			bo.setUrl((String) uploadResult.get("url"));
			bo.setPublicId((String)uploadResult.get("public_id"));
			bo.setFileName((String)uploadResult.get("original_filename"));
			bo.setFileExtension((String)uploadResult.get("format"));
			bannerRepository.save(bo);
			proxyBannerService.clearBannerCache();
		}
	}

	@Override
	@CacheEvict(cacheNames = CacheNames.BANNER_CACHE, key = "'banners'")
	public void clearBannerCache() {
		// Cache Evict method. No implementation required
	}
}
