package com.mani.project.auction.dto;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class BannerDto {

	private Integer id;
	
	private String url;
	
	private String publicId;
	
	private String fileName;
	
	private String fileExtension;
}
