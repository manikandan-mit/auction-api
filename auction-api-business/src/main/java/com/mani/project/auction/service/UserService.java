package com.mani.project.auction.service;

import java.util.List;

import com.mani.project.auction.dto.UserDto;

public interface UserService {
	public UserDto findUserById(String userId);
	public List<UserDto> getusers();
	public void updateActiveStatusOfUser(String userId, boolean activeStatus);
}
