package com.mani.project.auction.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ArtistDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private String artistId;
	private String artistName;
	private String artistDesc;
	private String artistProfileUrl;
	private String artistPublicId;
	private Boolean active;
	
}
