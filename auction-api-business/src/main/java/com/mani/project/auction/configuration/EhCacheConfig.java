package com.mani.project.auction.configuration;

import java.util.concurrent.TimeUnit;

import javax.annotation.PreDestroy;
import javax.cache.CacheManager;
import javax.cache.Caching;
import javax.cache.configuration.MutableConfiguration;
import javax.cache.expiry.CreatedExpiryPolicy;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.jcache.JCacheCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@EnableCaching
@Profile("cache")
public class EhCacheConfig {
	
	private CacheManager cacheManager;
	
	@Value("${category.cache.expiry.seconds:86400}")
	private int categoryCacheExpiry;
	
	@Value("${banner.cache.expiry.seconds:86400}")
	private int bannerCacheExpiry;

	@Value("${artist.cache.expiry.seconds:86400}")
	private int artistCacheExpiry;

	@PreDestroy
	public void destroy() {
		cacheManager.close();
	}
	
	@Bean
	public org.springframework.cache.CacheManager cacheManager() {
		cacheManager = this.getCacheManager();
		return new JCacheCacheManager(cacheManager);
	}
	
	private CacheManager getCacheManager() {
		CacheManager cacheMgr = Caching.getCachingProvider().getCacheManager();
		populateExpiryPolicy(cacheMgr);
		return cacheMgr;
	}
	
	private void populateExpiryPolicy(CacheManager cacheManager) {
		MutableConfiguration<Object, Object> categoryCacheConfig = new MutableConfiguration<Object, Object>()
				.setExpiryPolicyFactory(CreatedExpiryPolicy.factoryOf(new javax.cache.expiry.Duration(TimeUnit.SECONDS, categoryCacheExpiry)));
		createIfNotExist(cacheManager, CacheNames.CATEGORY_CACHE, categoryCacheConfig);

		MutableConfiguration<Object, Object> bannerCacheConfig = new MutableConfiguration<Object, Object>()
				.setExpiryPolicyFactory(CreatedExpiryPolicy.factoryOf(new javax.cache.expiry.Duration(TimeUnit.SECONDS, bannerCacheExpiry)));
		createIfNotExist(cacheManager, CacheNames.BANNER_CACHE, bannerCacheConfig);

		MutableConfiguration<Object, Object> artistCacheConfig = new MutableConfiguration<Object, Object>()
				.setExpiryPolicyFactory(CreatedExpiryPolicy.factoryOf(new javax.cache.expiry.Duration(TimeUnit.SECONDS, artistCacheExpiry)));
		createIfNotExist(cacheManager, CacheNames.ARTIST_CACHE, artistCacheConfig);
}
	
	private void createIfNotExist(CacheManager cacheManager, String cacheName, MutableConfiguration<Object, Object> configuration) {
		if(cacheManager.getCache(cacheName) == null) {
			cacheManager.createCache(cacheName, configuration);
		}
	}
}
