package com.mani.project.auction.mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.mani.project.auction.dto.BannerDto;
import com.mani.project.auction.entity.BannerBO;

@Component
public class BannerMapper {

	public List<BannerDto> mapBannerBoToDto(List<BannerBO> bannerBoList) {
		if(bannerBoList == null)
			return new ArrayList<>();
		
		return bannerBoList.stream().map(bo -> {
			BannerDto dto = new BannerDto();
			dto.setId(bo.getId());
			dto.setUrl(bo.getUrl());
			dto.setPublicId(bo.getPublicId());
			dto.setFileName(bo.getFileName());
			dto.setFileExtension(bo.getFileExtension());
			return dto;
		}).collect(Collectors.toList());
	}
}
