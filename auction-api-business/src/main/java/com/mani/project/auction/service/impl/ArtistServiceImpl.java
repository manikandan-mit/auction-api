package com.mani.project.auction.service.impl;

import java.io.File;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.mani.project.auction.configuration.CacheNames;
import com.mani.project.auction.dto.ArtistDto;
import com.mani.project.auction.entity.ArtistBO;
import com.mani.project.auction.entity.interfaces.ArtistBasicInfo;
import com.mani.project.auction.exception.ResourceNotFoundException;
import com.mani.project.auction.mapper.ArtistMapper;
import com.mani.project.auction.repository.ArtistRepository;
import com.mani.project.auction.service.ArtistService;

@Service
public class ArtistServiceImpl implements ArtistService {

	@Autowired
	private ArtistRepository artistRepository;
	
	@Autowired
	private ArtistMapper artistMapper;
	
	@Autowired
	private DozerBeanMapper dozerMapper;
	
	@Autowired
	private ArtistService proxyArtistService;

	@Autowired
	private Cloudinary cloudinaryService;

	private static final String PATH_DELIMITER = "/";

	private static Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

//	@Autowired
//	private EncryptionUtil encryptionUtil;
	
	@Override
	public List<ArtistDto> getAllArtists(boolean includeInactive) {
		List<ArtistDto> artistList =  proxyArtistService.getAllArtistFromDB();
		if(!includeInactive) {
			artistList = artistList.stream().filter(artist -> artist.getActive()).collect(Collectors.toList());
		}
		return artistList;
	}

	@Override
	public ArtistDto getArtistById(String encArtistId, boolean populateDefaultProfile) {
		int artistId = Integer.parseInt(encArtistId);
		ArtistBO artist = artistRepository.findById(artistId).orElseThrow(() -> new ResourceNotFoundException("Artist not found"));
		return artistMapper.mapArtistBoToDto(artist, populateDefaultProfile);
	}
	
	@Override
	@Cacheable(cacheNames = CacheNames.ARTIST_CACHE, key = "'artist'", unless = "#result == null")
	public List<ArtistDto> getAllArtistFromDB() {
		List<ArtistBasicInfo> artistList = artistRepository.findBy();
		return artistList.stream().map(artist -> dozerMapper.map(artist, ArtistDto.class)).collect(Collectors.toList());
	}
	
	@Override
	@CacheEvict(cacheNames = CacheNames.ARTIST_CACHE, key = "'artist'")
	public void clearArtistCache() {
		// Cache Evict method. No implementation required
	}

	@Override
	public void updateActiveStatusOfArtist(int artistId, boolean activeStatus) {
		artistRepository.updateActiveStatus(artistId, activeStatus);
		proxyArtistService.clearArtistCache();
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void saveArtist(MultipartFile file, ArtistDto artistInfo, String deleteId) throws Exception {
		if(deleteId != null) {
			cloudinaryService.uploader().destroy(deleteId, ObjectUtils.emptyMap());
		}
		ArtistBO artist = dozerMapper.map(artistInfo, ArtistBO.class);
		artist.setActive(true);
//		MultipartFile file = artistRequest.getFile();
		Map uploadResult = null;
		if(file != null) {
			File convFile = new File(System.getProperty("java.io.tmpdir") + PATH_DELIMITER + file.getOriginalFilename());
			try {
				file.transferTo(convFile);
				uploadResult = cloudinaryService.uploader().upload(convFile,
				        ObjectUtils.asMap("resource_type", "auto"));
			} catch (IllegalStateException | IOException e) {
				logger.error("File upload failed");
				throw e;
			}
		}
		
		if(uploadResult != null) {
			artist.setArtistProfileUrl((String) uploadResult.get("url"));
			artist.setArtistPublicId((String)uploadResult.get("public_id"));
		}
		
		artistRepository.save(artist);
		proxyArtistService.clearArtistCache();
	}
	
	@Override
	public List<ArtistDto> listAllActiveArtist() {
		List<Object[]> result = artistRepository.listAllActiveArtist();
		return result.stream().map(obj -> {
			ArtistDto artist = new ArtistDto();
			artist.setArtistId(String.valueOf((Integer) obj[0]));
			artist.setArtistName((String) obj[1]);
			return artist;
		}).collect(Collectors.toList());
	}

}
