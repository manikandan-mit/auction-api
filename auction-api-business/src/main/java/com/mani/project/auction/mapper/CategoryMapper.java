package com.mani.project.auction.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.mani.project.auction.dto.CategoryDto;
import com.mani.project.auction.dto.SubCategoryDto;
import com.mani.project.auction.entity.CategoryBO;
import com.mani.project.auction.entity.SubCategoryBO;

@Component
public class CategoryMapper {

	public List<CategoryDto> mapCategoryBoToDto(List<CategoryBO> categoryBO) {
		
		return categoryBO.stream().map(bo -> {
			CategoryDto categoryDto = new CategoryDto();
			categoryDto.setCategoryId(bo.getCategoryId());
			categoryDto.setCategoryName(bo.getCategoryName());
			categoryDto.setActiveFlag(bo.isActive());
			categoryDto.setSubCategory(
				bo.getSubCategoryBO().stream().map(CategoryMapper::mapSubCategoryBoToDto).sorted().collect(Collectors.toList())
			);
			return categoryDto;
		}).collect(Collectors.toList());
		
	}
	
	private static SubCategoryDto mapSubCategoryBoToDto(SubCategoryBO subCategoryBo) {
		SubCategoryDto subCategoryDto = new SubCategoryDto();
		subCategoryDto.setSubCategoryId(subCategoryBo.getSubCategoryId());
		subCategoryDto.setSubCategoryName(subCategoryBo.getSubCategoryName());
		subCategoryDto.setOrder(subCategoryBo.getOrder());
		subCategoryDto.setActiveFlag(subCategoryBo.getIsActive());
		return subCategoryDto;
	}
	
	public SubCategoryBO mapSubCategoryDtoToBo(SubCategoryDto subCategoryDto) {
		SubCategoryBO subCategoryBO = new SubCategoryBO();
		subCategoryBO.setSubCategoryName(subCategoryDto.getSubCategoryName());
		subCategoryBO.setOrder(0);
		subCategoryBO.setIsActive(true);
		return subCategoryBO;
	}
	
	public CategoryBO mapCategoryDtoToBo(CategoryDto categoryDto) {
		CategoryBO categoryBo = new CategoryBO();
		categoryBo.setCategoryName(categoryDto.getCategoryName());
		categoryBo.setActive(true);
//		if(!categoryDto.getSubCategory().isEmpty()) {
//			categoryBo.setSubCategoryBO(new ArrayList<SubCategoryBO>());
//			categoryDto.getSubCategory().forEach(sub -> {
//				SubCategoryBO subCategoryBO = mapSubCategoryDtoToBo(sub);
//				subCategoryBO.setCategory(categoryBo);
//				categoryBo.getSubCategoryBO().add(subCategoryBO);
//			});
//		}
		return categoryBo;
	}
}
