package com.mani.project.auction.service;

import java.util.List;

import com.mani.project.auction.dto.CategoryDto;
import com.mani.project.auction.dto.SubCategoryDto;
import com.mani.project.auction.entity.CategoryBO;

public interface CategoryService {

	public List<CategoryDto> findAllCategories(boolean includeInactiveCategories);
	public List<CategoryBO> getCategoryInfoFromDB();
	public List<SubCategoryDto> listAllActiveSubCategory();
	public void updateActiveStatusOfCategories(int categoryId, boolean activeStatus);
	public void updateActiveStatusOfSubCategories(int subCategoryId, boolean activeStatus);
	public void clearCategoryCache();
	
	public void addCategory(CategoryDto categoryDto);
	public void addSubCategory(SubCategoryDto subCategoryDto);
}
