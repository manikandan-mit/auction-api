package com.mani.project.auction.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class CategoryDto {

	private Integer categoryId;
	private String categoryName;
	private Boolean activeFlag;
	private List<SubCategoryDto> subCategory;
}
