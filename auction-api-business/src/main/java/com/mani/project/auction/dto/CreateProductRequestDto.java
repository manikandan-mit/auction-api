package com.mani.project.auction.dto;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class CreateProductRequestDto {
	private Integer productId;
	private String productName;
	private String productDescription;
	private Date auctionDate;
	private Integer estimatedMin;
	private Integer estimatedMax;
	private Integer artistId;
	private Integer subCategoryId;
	private String additionalInfo;
}
