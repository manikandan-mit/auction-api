package com.mani.project.auction.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.mani.project.auction.dto.UserDetailsDto;
import com.mani.project.auction.dto.UserDto;
import com.mani.project.auction.entity.UserBO;
import com.mani.project.auction.entity.UserRole;
import com.mani.project.auction.mapper.UserDetailsMapper;
import com.mani.project.auction.repository.UserRepository;
import com.mani.project.auction.service.UserService;

@Service
public class UserServiceImpl implements UserDetailsService, UserService {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private UserDetailsMapper userMapper;
	
	@Override
	public UserDetailsDto loadUserByUsername(String username) {
		UserBO user = userRepository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException("User not found"));
		return UserDetailsDto.build(user);
	}

	@Override
	public UserDto findUserById(String userId) {
		UserBO userBO = userRepository.findByUserId(userId).orElseThrow(() -> new UsernameNotFoundException("User not found"));
		return userMapper.mapUserBoToDto(userBO);
	}

	@Override
	public List<UserDto> getusers() {
		List<UserBO> users = userRepository.findByRolesRoleName(UserRole.USER);
		return users.stream().map(user -> userMapper.mapUserBoToDto(user)).collect(Collectors.toList());
	}
	
	@Override
	public void updateActiveStatusOfUser(String userId, boolean activeStatus) {
		userRepository.updateActiveStatusOfUser(userId, activeStatus);
	}

}
