package com.mani.project.auction.util;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.dozer.DozerConverter;

public class DozerCustomConverter extends DozerConverter<Map, String> {

	public DozerCustomConverter(Class<Map> prototypeA, Class<String> prototypeB) {
		super(prototypeA, prototypeB);
	}

	@Override
	public Map<String, String> convertFrom(String source, Map destination) {
		if(source == null)
			return null;
		Map<String, String> additionalInfo = new HashMap<String, String>();
		String[] data = source.split("|");
		Arrays.asList(data).stream().forEach(datum -> 	additionalInfo.put(datum.split("=")[0], datum.split("=")[1]));
		return additionalInfo;
	}

	@Override
	public String convertTo(Map source, String destination) {
		if(source == null)
			return null;		
		Map<String, String> additionalInfo = source;
		String additionalInfoString = additionalInfo.keySet().stream()
																	.map(key -> key.concat("=").concat(additionalInfo.get(key)))
																	.collect(Collectors.joining("|"));
		return additionalInfoString;
	}
}
