package com.mani.project.auction.configuration;

import java.util.Arrays;
import java.util.Map;

import org.dozer.DozerBeanMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.mani.project.auction.util.DozerCustomConverter;

@Configuration
public class DozerConfig {
	
	@Bean
	public DozerBeanMapper dozerMapper() {
		DozerBeanMapper dozerMapper = new DozerBeanMapper();
		dozerMapper.setCustomConverters(Arrays.asList(new DozerCustomConverter(Map.class, String.class)));
//		List<String> mappingFiles = Arrays.asList("dozer-configuration.xml");
//		dozerMapper.setMappingFiles(mappingFiles);
		return dozerMapper;
	}

}
