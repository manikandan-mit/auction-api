package com.mani.project.auction.dto;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ProductDto {
	
	private Integer productId;
	private String productName;
	private String productDescription;
	private Date createdDate; 
	private Date auctionDate;
	private Integer estimatedMin;
	private Integer estimatedMax;
	private String imageUrl;
	private ArtistDto artist;
	private SubCategoryDto category;
	private String additionalInfo;
	private Long timeRemaining;
}
