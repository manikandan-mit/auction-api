package com.mani.project.auction.service.impl;

import java.io.File;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.mani.project.auction.dto.CreateProductRequestDto;
import com.mani.project.auction.dto.ProductDto;
import com.mani.project.auction.dto.ProductResponseDto;
import com.mani.project.auction.entity.ArtistBO;
import com.mani.project.auction.entity.ProductBO;
import com.mani.project.auction.entity.StatusBO;
import com.mani.project.auction.entity.SubCategoryBO;
import com.mani.project.auction.entity.UserBO;
import com.mani.project.auction.exception.ResourceNotFoundException;
import com.mani.project.auction.repository.ProductRepository;
import com.mani.project.auction.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private DozerBeanMapper dozerMapper;

	@Autowired
	private Cloudinary cloudinaryService;

	private static final String PATH_DELIMITER = "/";

	private static Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	@Override
	public ProductResponseDto findAllProducts(List<Integer> categoryId, List<Integer> artistId, int pageNo, int pageSize) {
		int pageIndex = (pageNo -1) < 0? 0 : (pageNo -1);
		Pageable paging = PageRequest.of(pageIndex, pageSize, Sort.by("auctionDate").ascending()); 
		Page<ProductBO> response = null;

		if(CollectionUtils.isNotEmpty(categoryId) && CollectionUtils.isNotEmpty(artistId)) {
			response = productRepository.findProductsByArtistAndCategory(artistId, categoryId, paging);
		}else if(CollectionUtils.isNotEmpty(categoryId)) {
			response = productRepository.findProductsByCategory(categoryId, paging);
		} else if(CollectionUtils.isNotEmpty(artistId)) {
			response = productRepository.findProductsByArtist(artistId, paging);
		} else {
			response = productRepository.findAllProducts(paging);
		}

		List<ProductDto> products = response.getContent().stream().map(prd -> dozerMapper.map(prd, ProductDto.class)).collect(Collectors.toList());
		
		products.forEach(prd -> {
			long temp = prd.getAuctionDate().getTime() - new Date().getTime();
			if(temp < 5400000) {
				prd.setTimeRemaining(temp);
			}
		});
		
		ProductResponseDto result = new ProductResponseDto();
		result.setProducts(products);
		result.setPageNumber(response.getNumber()+1);
		result.setProductsPerPage(pageSize);
		result.setTotalNumberOfElements(response.getTotalElements());
		return result;
	}
	
	@Override
	public ProductResponseDto listAllProducts(int pageNo, int pageSize, Integer status)  {
		int pageIndex = (pageNo -1) < 0? 0 : (pageNo -1);
		Pageable paging = PageRequest.of(pageIndex, pageSize, Sort.by("auctionDate").ascending()); 
		Page<Object[]> response = productRepository.listAllProducts(status, paging);
		List<ProductDto> products = response.getContent().stream().map(objArray -> {
																	ProductDto p = new ProductDto();
																	p.setProductId((Integer) objArray[0]);
																	p.setProductName((String) objArray[1]);
																	p.setAuctionDate((Date) objArray[2]);
																	return p;
																}).collect(Collectors.toList());
		ProductResponseDto result = new ProductResponseDto();
		result.setProducts(products);
		result.setPageNumber(response.getNumber()+1);
		result.setProductsPerPage(pageSize);
		result.setTotalNumberOfElements(response.getTotalElements());
		return result;
	}

	@Override
	public ProductDto findProductById(int productId) {
		ProductBO product = productRepository.findById(productId).orElseThrow(() -> new ResourceNotFoundException("Requested product is not found"));
		return dozerMapper.map(product, ProductDto.class);
	}

	@Override
	public void updateProductStatus(List<Integer> productIds, Integer status) {
		productRepository.updateProductStatus(productIds, status);
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public void saveProduct(MultipartFile file, CreateProductRequestDto product, String userId) throws Exception {
		ProductBO productBO = new ProductBO();
		productBO.setProductName(product.getProductName());
		productBO.setProductDescription(product.getProductDescription());
		productBO.setCreatedDate(new Date());
		
		UserBO user = new UserBO();
		user.setUserId(userId);
		productBO.setUser(user);

		productBO.setAuctionDate(new Timestamp(product.getAuctionDate().getTime()));
		
		ArtistBO artist = new ArtistBO();
		artist.setArtistId(product.getArtistId());
		productBO.setArtist(artist);
		
		productBO.setEstimatedMin(product.getEstimatedMin());
		productBO.setEstimatedMax(product.getEstimatedMax());

		StatusBO status = new StatusBO();
		status.setStatusId(1);
		productBO.setStatus(status); 
		
		SubCategoryBO category = new SubCategoryBO();
		category.setSubCategoryId(product.getSubCategoryId());
		productBO.setCategory(category);
		
		productBO.setAdditionalInfo(product.getAdditionalInfo());
		
		File convFile = new File(System.getProperty("java.io.tmpdir") + PATH_DELIMITER + file.getOriginalFilename());
		Map uploadResult = null;
		try {
			file.transferTo(convFile);
			uploadResult = cloudinaryService.uploader().upload(convFile,
			        ObjectUtils.asMap("resource_type", "auto"));
		} catch (IllegalStateException | IOException e) {
			logger.error("File upload failed");
			throw e;
		}

		if(uploadResult != null) {
			productBO.setImageUrl((String) uploadResult.get("url"));
			productBO.setImageId((String)uploadResult.get("public_id"));
		}
		
		productRepository.save(productBO);
	}

	@Override
	public void updateProductName(String productName, Integer productId) {
		productRepository.updateProductName(productName, productId);
	}

	@Override
	public void updateProductDesc(String productDescription, Integer productId) {
		productRepository.updateProductDesc(productDescription, productId);
	}

	@Override
	public void updateProductEstimate(Integer estimatedMin, Integer estimatedMax, Integer productId) {
		productRepository.updateProductEstimate(estimatedMin, estimatedMax, productId);
	}

	@Override
	public void updateProductAddInfo(String additionalInfo, Integer productId) {
		productRepository.updateProductAddInfo(additionalInfo, productId);
	}
	
	@Override
	public void updateProductAuctionDate(String auctionDate, Integer productId) throws ParseException {
		SimpleDateFormat sp = new SimpleDateFormat("dd-M-yyyy hh:mm");
		productRepository.updateProductAuctionDate(new Timestamp(sp.parse(auctionDate).getTime()), productId);
	}
}
