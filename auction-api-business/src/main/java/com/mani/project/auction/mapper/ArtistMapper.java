package com.mani.project.auction.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.mani.project.auction.dto.ArtistDto;
import com.mani.project.auction.entity.ArtistBO;

@Service
public class ArtistMapper {

	@Value("${artist.default.profile.url}")
	private String defaultProfileImg;

	@Value("${artist.default.profile.id}")
	private String defaultProfileId;

//	@Autowired
//	private EncryptionUtil encryptionUtil;

	public ArtistDto mapArtistBoToDto(ArtistBO artistBO, boolean populateDefaultProfile) {
		ArtistDto artistDto = new ArtistDto();
		String artistId = String.valueOf(artistBO.getArtistId());
		artistDto.setArtistId(artistId);
		artistDto.setArtistName(artistBO.getArtistName());
		artistDto.setArtistDesc(artistBO.getArtistDesc());
		if(artistBO.getArtistProfileUrl() != null) {
			artistDto.setArtistProfileUrl(artistBO.getArtistProfileUrl());
			artistDto.setArtistPublicId(artistBO.getArtistPublicId());
		} else if(populateDefaultProfile) {
			artistDto.setArtistProfileUrl(defaultProfileImg);
			artistDto.setArtistPublicId(defaultProfileId);
		}
		artistDto.setActive(artistBO.getActive());
		return artistDto;
	}
	
	public List<ArtistDto> mapArtistBoToDto(List<ArtistBO> artistBoList) {
		return artistBoList
							.stream()
							.map(bo -> mapArtistBoToDto(bo, false))
							.collect(Collectors.toList());
	}
}
