package com.mani.project.auction.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter @EqualsAndHashCode
public class SubCategoryDto implements Comparable<SubCategoryDto> {

	private Integer subCategoryId;
	private String subCategoryName;
	private Boolean activeFlag;
	private Integer order;
	private Integer categoryId;
	
	@Override
	public int compareTo(SubCategoryDto o) {
		if(this.getOrder() > o.getOrder()) {
			return 1;
		}else if(this.getOrder() < o.getOrder()) {
			return -1;
		}
		return 0;
	}
}
